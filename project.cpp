#include<iostream>
#include <math.h>
using namespace std;

int main()
{
    float fahrenheit, celsius, circumference,
    radius, area, rectangle, width, length, height;
    float side1, side2, side3, s, volume;
    int choice;

    do
    {
            cout<<"1.Convert Celsius to Fahrenheit"<<endl;
            cout<<"2.Convert Fahrenheit to Celsius"<<endl;
            cout<<"3.Calculate Circumference of a circle"<<endl;
            cout<<"4.Calculate Area of a circle"<<endl;
            cout<<"5.Area of Rectangle"<<endl;
            cout<<"6.Area of Triangle (Heron�s Formula)"<<endl;
            cout<<"7.Volume of Cylinder"<<endl;
            cout<<"8.Volume of Cone"<<endl;
            cout<<"9.Quit program"<<endl;
            cout<<"Please enter the choice: "<<endl;
            cin>>choice;

            if(choice==1)
                {
                    cout << "Enter the temperature in Celsius : ";
                    cin >> celsius;
                    fahrenheit = (celsius * 9.0) / 5.0 + 32;
                    cout << "The temperature in Fahrenheit : " << fahrenheit << endl;
                }
            else if(choice==2)
                {
                    cout << "Enter the temperature in Fahrenheit : ";
                    cin >> fahrenheit;
                    celsius =  (fahrenheit-32)*5/9;
                    cout << "The temperature in Celsius: " << celsius << endl;
                }
            else if(choice==3)
                {
                    cout << "Enter the radius of circle : ";
                    cin >> radius;
                    circumference = (2 * 3.14 * radius);
                    cout << "The circumference of circle is " << circumference << endl;
                }
            else if(choice==4)
                {
                    cout << "Enter the radius of circle in : ";
                    cin >> radius;
                    area = 3.14 * (radius * radius);
                    cout << "The area of circle is " << area << endl;
                }
            else if(choice==5)
                {
                    cout << "Enter length of rectangle : ";
                    cin >> length;
                    cout << "Enter the width of rectangle : ";
                    cin >> width;
                    rectangle = width * length;
                    cout << "The area of rectangle is " << rectangle << endl;
                }
            else if(choice==6)
                {
                    cout << "Enter 1st side of triangle :";
                    cin >> side1;
                    cout << "Enter 2nd side of triangle :";
                    cin >> side2;
                    cout << "Enter 3rd side of triangle :";
                    cin >> side3;
                    s = (side1+side2+side3)/2;
                    area = sqrt(s*(s-side1)*(s-side2)*(s-side3));
                    cout<<" The area of the triangle in Heron's Formula is : "<< area << endl;
                }
            else if(choice==7)
                {
                    cout << "Enter the radius of the cylinder : ";
                    cin >> radius;
                    cout << "Enter the height of the cylinder : ";
                    cin >> height;
                    volume = (3.14*radius*radius*height);
                    cout << "The volume of the cylinder is : " << volume << endl;
                }
            else if(choice==8)
                {
                    cout << "Enter the radius of cone: ";
                    cin >> radius;
                    cout << "Enter the height of cone: ";
                    cin >> height;
                    volume = (1/3) * 3.14 * (radius*radius) * height;
                    cout << "The volume of the cone is " << volume << endl;
                }
    }while(choice>9);

    return 0;
}
